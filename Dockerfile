FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.2-sdk AS publish
WORKDIR /src
COPY . .
RUN dotnet restore testImage.csproj
RUN dotnet build testImage.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /src .
ENTRYPOINT ["dotnet","testImage.dll"]




